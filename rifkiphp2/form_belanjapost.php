<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Belanja Online</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

  </head>
  <body>

    <form class="form-horizontal" method="POST" action="nilai_belanjapost.php">
<fieldset>

  <div class="container-fluid">
  	<div class="row">
  		<div class="col-md-8">
  		
        <form class="form-horizontal">
<fieldset>

<!-- Form Name -->
<legend>Belanja Online</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="cstmr">Customer</label>  
  <div class="col-md-4">
  <input id="cstmr" name="cstmr" type="text" placeholder="" class="form-control input-md">
    
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="produk">Pilih Produk</label>
  <div class="col-md-4"> 
    <label class="radio-inline" for="produk-0">
      <input type="radio" name="produk" id="produk-0" value="TV" checked="checked">
      TV
    </label> 
    <label class="radio-inline" for="produk-1">
      <input type="radio" name="produk" id="produk-1" value="KULKAS">
      KULKAS
    </label> 
    <label class="radio-inline" for="produk-2">
      <input type="radio" name="produk" id="produk-2" value="MESIN CUCI">
      MESIN CUCI
    </label>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="jml">Jumlah</label>  
  <div class="col-md-2">
  <input id="jml" name="jml" type="text" placeholder="Jumlah" class="form-control input-md">
    
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="kirim"></label>
  <div class="col-md-4">
    <button id="kirim" name="kirim" class="btn btn-success">Kirim</button>
  </div>
</div>

</fieldset>
</form>

  		</div>
  		<div class="col-md-4">
  			<div class="list-group">
  				 <a href="#" class="list-group-item active">Daftar Harga</a>
  				<div class="list-group-item">
  					TV = 4.200.000
  				</div>
  				<div class="list-group-item">
  					<p class="list-group-item-heading">
  						KULKAS = 3.100.000
  					</p>
  					</div>
  				<div class="list-group-item">
            MESIN CUCI = 3.800.000
  				</div> <a class="list-group-item active">Harga dapat berubah</a>
  			</div>
  		</div>
  	</div>
  </div>


    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>


</body>
</html>




